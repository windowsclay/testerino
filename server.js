const express = require('express')
const bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient
const bcrypt = require('bcryptjs')
const jwt = require('jwt-simple')
const jwtSecret = 'testbrah'

const app = express()

app.use(express.static('public'))

var db
MongoClient.connect('mongodb://localhost:27017/testerino', (err, database) => {
  if(err) return console.log(err);
  db = database
  app.listen(3000, () => {
    console.log('listening on 3000')
  })
})

app.use(bodyParser.json());


// get the list of all projects
app.get('/projects', function (req, res) {
  var cursor = db.collection('projects').find()
  db.collection('projects').find().toArray(function (err, projects) {
    return res.send(projects);
  });
});

// make new project
app.post('/projects', function(req, res) {
  var token = req.headers.authorization;
  var user = jwt.decode(token, jwtSecret);
  db.collection('projects', function(err, projectsCollection) {
    var newProject = {
      projectTitle: req.body.projectTitle,
      projectDescription: req.body.projectDescription,
      user: user._id,
      username: user.username
    };
    projectsCollection.insert(newProject, function(err) {
      if (err) return console.log(err);
      res.send();
    });
  });
});

// register
app.post('/users', function(req, res) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(req.body.password, salt, function(err, hash) {
      var newUser = {
        username: req.body.username,
        password: hash
      }
      db.collection('users').save(newUser, (err, result) => {
        if (err) return console.log(err);
        res.send();
      });
    });
  });
});

// sign in
app.post('/users/signin', function(req, res) {
  db.collection('users').findOne( {username: req.body.username}, function(err, user) {
    bcrypt.compare(req.body.password, user.password, function(err, result) {
      if (result) {
        var token = jwt.encode(user, jwtSecret);
        return res.json({ token: token });
      } else {
        return res.status(403);
      }
    });
  });
});
