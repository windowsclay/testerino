var app = angular.module('testerino', ['ngRoute', 'ngCookies']);

app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'mainCtrl'
    })
    .when('/register', {
      templateUrl: 'views/register.html',
      controller: 'registerCtrl'
    })
});

app.run( function($rootScope, $cookies) {
  if($cookies.get('token') && $cookies.get('currentUser')) {
    $rootScope.token = $cookies.get('token');
    $rootScope.currentUser = $cookies.get('currentUser');
  }
});

app.controller('mainCtrl', function mainCtrl($rootScope, $scope, $http, $cookies) {

  $scope.createProject = function() {
    $http.post('/projects', { projectTitle: $scope.pt, projectDescription: $scope.pd }, {headers: {'authorization': $rootScope.token} } ).then(function() {
      getProjects();
    })
  }

  $scope.signin = function() {
    $http.post('/users/signin', {username: $scope.un, password: $scope.pw})
    .then(function(res) {
      $cookies.put('token', res.data.token);
      $cookies.put('currentUser', $scope.un);
      $rootScope.token = res.data.token;
      $rootScope.currentUser = $scope.un;
    }, function(err) {
      alert('bad credentials');
    });
  }

  $scope.logout = function() {
    $cookies.remove('token');
    $cookies.remove('currentUser');
    $rootScope.token = null;
    $rootScope.currentUser = null;
  }

  function getProjects() {
    $http.get('/projects').then(function(response) {
      $scope.projects = response.data;
    });
  }
  getProjects();
});

app.controller('registerCtrl', function mainCtrl($scope, $http) {
  $scope.createUser = function() {
    var newUser = {
      username: $scope.un,
      password: $scope.pw
    }
    $http.post('/users', newUser).then(function() {
      alert('success');
    });
  }
});
